package kz.aitu.chat.controller;

import kz.aitu.chat.model.Chat;
import kz.aitu.chat.service.ChatService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/api/chat")
public class ChatController {
    private final ChatService chatService;

    @GetMapping("")
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(chatService.getAll());
    }

    @PostMapping("")
    public ResponseEntity<?> add(@RequestBody Chat chat) {
        chatService.add(chat);
        return ResponseEntity.ok("Chat was added!");
    }

    @PutMapping("")
    public ResponseEntity<?> update(@RequestBody Chat chat) {
        try {
            chatService.update(chat);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.ok(chat);
    }

    @DeleteMapping("delete/{chatId}")
    public ResponseEntity<?> deleteById(@PathVariable Long chatId) {
        try {
            chatService.deleteById(chatId);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.ok("Chat was deleted!");
    }
}
