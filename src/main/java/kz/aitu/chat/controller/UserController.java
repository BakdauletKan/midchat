package kz.aitu.chat.controller;

import kz.aitu.chat.model.User;
import kz.aitu.chat.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("api/user")
public class UserController {
    private final UserService userService;

    @GetMapping("")
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(userService.getAll());
    }

    @PostMapping("")
    public ResponseEntity<?> add(@RequestBody User user) {
        userService.add(user);

        return ResponseEntity.ok("User was added!");
    }

    @PutMapping("")
    public ResponseEntity<?> update(@RequestBody User user) {
        try {
            userService.update(user);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.ok(user);
    }

    @DeleteMapping("")
    public ResponseEntity<?> delete(@RequestBody User user) {
        try {
            userService.delete(user);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.ok("User was deleted!");
    }
}
