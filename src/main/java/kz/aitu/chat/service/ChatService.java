package kz.aitu.chat.service;

import kz.aitu.chat.model.Chat;
import kz.aitu.chat.repository.ChatRepository;
import kz.aitu.chat.service.interfaces.IChatService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class ChatService implements IChatService {
    private final ChatRepository chatRepository;

    @Override
    public void add(Chat o) {
        chatRepository.save(o);
    }

    @Override
    public List<Chat> getAll() {
        return chatRepository.findAll();
    }

    @Override
    public void update(Chat chat) {
        chatRepository.save(chat);
    }

    @Override
    public void delete(Chat chat) {
        chatRepository.delete(chat);
    }

    @Override
    public void deleteById(Long id) {
        chatRepository.deleteById(id);
    }

    @Override
    public Chat getItemById(Long id) {
        return chatRepository.findById(id).get();
    }


}
