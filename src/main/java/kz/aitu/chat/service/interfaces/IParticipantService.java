package kz.aitu.chat.service.interfaces;

import kz.aitu.chat.model.Chat;
import kz.aitu.chat.model.Participant;
import kz.aitu.chat.model.User;

import java.util.List;

public interface IParticipantService extends IService<Participant> {
    List<Chat> getChatsByUserId(Long userId);
    List<User> getUsersByChatId(Long chatId);

}
