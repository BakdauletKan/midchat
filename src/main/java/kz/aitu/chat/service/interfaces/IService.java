package kz.aitu.chat.service.interfaces;

import java.util.List;

public interface IService<T> {
    void add(T o);
    List<T> getAll();
    void update(T t);
    void delete(T t);
    void deleteById(Long id);
    T getItemById(Long id);
}
