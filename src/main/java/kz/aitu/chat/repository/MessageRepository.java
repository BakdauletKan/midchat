package kz.aitu.chat.repository;

import kz.aitu.chat.model.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MessageRepository extends JpaRepository<Message, Long> {
    List<Message> findMessagesByChatId(Long chatId);

    @Query(value = "SELECT * FROM message m WHERE m.chat_id = ?1 ORDER BY m.id LIMIT 10", nativeQuery=true)
    List<Message> getLast10ByChatId(Long id);

    @Query(value = "SELECT * FROM message m WHERE m.user_id = ?1 ORDER BY m.id LIMIT 10", nativeQuery=true)
    List<Message> getLast10ByUserId(Long id);

}
