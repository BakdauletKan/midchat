package kz.aitu.chat.repository;

import kz.aitu.chat.model.Chat;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface ChatRepository extends JpaRepository<Chat, Long> {

}
